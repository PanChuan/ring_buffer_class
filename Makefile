EXE=test
CC=g++
OBJ=test.o fixed_allocator.o
CFLAGS=-g  -lstdc++ -lpthread

$(EXE):$(OBJ)
	$(CC) $^ -o $@ $(CFLAGS)
clean:
	rm $(EXE) $(OBJ) -f
