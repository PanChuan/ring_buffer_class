#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "fixed_allocator.h"

FixedAllocator::FixedAllocator(int _item_num, int _item_size):
		item_size(_item_size), capacity(_item_num) {
		
	// initialize the atomic operator
	pthread_mutex_init(&alloc_mutex, NULL);
	pthread_cond_init(&alloc_cond, NULL);
	
	//initialize the buffer
	// round item_size to 4KB boundary
#define ITEM_BOUND (1024*4)
	if( (item_size % ITEM_BOUND ) != 0 ) {
	     item_size = item_size + ITEM_BOUND - (item_size % ITEM_BOUND);
	}
	arena = (char*)malloc( item_size * capacity  ); 
	
	//initilize free list
	unsigned long tt = (unsigned long)arena;
	for( int ci = capacity; ci > 0; ci--, tt += item_size ) {
	    free_list.push_back( (INT_ITTEM_P)tt );
	}
}

FixedAllocator::~FixedAllocator() {
    pthread_cond_destroy(&alloc_cond);
    pthread_mutex_destroy(&alloc_mutex);
	
    free(arena);
	arena = NULL;

    return;
}

void* FixedAllocator::allocItem() {
    void* ret;
    pthread_mutex_lock(&alloc_mutex);
    while (free_list.size() == 0 ) {
#ifdef DEBUG_THRD_MA
        printf("Buffer pool empty, waiting for outque!\n");fflush(stdout);
#endif
        pthread_cond_wait(&alloc_cond, &alloc_mutex);
    }
    ret = (void*)free_list.front();
    free_list.pop_front();
    pthread_mutex_unlock(&alloc_mutex);

    return ret;
}

void  FixedAllocator::freeItem(void* item) {
	pthread_mutex_lock(&alloc_mutex);
	free_list.push_back( (INT_ITTEM_P)item );
	pthread_cond_signal(&alloc_cond);
	pthread_mutex_unlock(&alloc_mutex);
	
    return;
}

