#ifndef _mt_queue_h_
#define _mt_queue_h_

#include <pthread.h>
#include <list>

using namespace std;

template <typename IT_T> class mtQueue
{
    pthread_mutex_t  mtq_mutex;
    list<IT_T>       m_queue;
//    int              m_que_cap;
    pthread_cond_t   mtq_add_cond; 
//    pthread_cond_t   mtq_rem_cond; 

  public:
    mtQueue() {
	//m_que_cap = cap;
        pthread_cond_init(&mtq_add_cond, NULL);
  //  pthread_cond_init(&mtq_rem_cond, NULL);
        pthread_mutex_init(&mtq_mutex, NULL);
    }
    ~mtQueue() {
        pthread_cond_destroy(&mtq_add_cond);
  //      pthread_cond_destroy(&mtq_rem_cond);
        pthread_mutex_destroy(&mtq_mutex);
    }

    void inQue(IT_T item) {
        pthread_mutex_lock(&mtq_mutex);
 /*       while (m_queue.size() == m_que_cap) {
#ifdef DEBUG_THRD_MA
	    printf("inQue, full, wait!\n");fflush(stdout);
#endif
            pthread_cond_wait(&mtq_rem_cond, &mtq_mutex);
        }
  */      
        m_queue.push_back(item);
        pthread_cond_signal(&mtq_add_cond);
        pthread_mutex_unlock(&mtq_mutex);
    }

    IT_T outQue() {
        pthread_mutex_lock(&mtq_mutex);
        while (m_queue.size() == 0) {
#ifdef DEBUG_THRD_MA
	    printf("outQue, empty, wait\n");fflush(stdout);
#endif
            pthread_cond_wait(&mtq_add_cond, &mtq_mutex);
        }
        IT_T item = m_queue.front();
        m_queue.pop_front();		
 //       pthread_cond_signal(&mtq_rem_cond);
        pthread_mutex_unlock(&mtq_mutex);
        return item;
    }

/*    int size() {
        //pthread_mutex_lock(&mtq_mutex);
        int size = m_queue.size();
        //pthread_mutex_unlock(&mtq_mutex);
        return size;
    }
*/
};

//typedef mtQueue<void*> RawImgQue;
//extern RawImgQue * raw_img_que;
typedef mtQueue<void*> RawFrameQue;
extern RawFrameQue *raw_frame_que; 


#endif

