#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<pthread.h>
#include "fixed_allocator.h"
#include "mt_queue.h"

#define BUF_SIZE (1280*720*3+128)
#define BUF_QUE_CAP 4
#define SIZE 100000

FixedAllocator *alloc_buf_que = NULL;
RawFrameQue * raw_frame_que = NULL;

int create_que()
{
	alloc_buf_que = new FixedAllocator(BUF_QUE_CAP,BUF_SIZE);
	if(alloc_buf_que == NULL)
	{
		fprintf(stderr,"alloc_buf_que creation failed!\n");
		exit(2);
	}

	raw_frame_que = new RawFrameQue();
	if(raw_frame_que == NULL)
	{
		fprintf(stderr,"raw_frame_que creation failed\n");
		exit(2);
	}

	return 0;
}

void * producer(void *ptr)
{
	char * str = "wwwwww";
	char *buf = (char *)malloc(SIZE);
	FILE * fp = fopen("data.txt","r");
	fgets(buf,SIZE-1,fp);
	void * ptt;
	int i = 0;
	while(i < 300)
	{
		ptt = alloc_buf_que->allocItem();
		printf("producer:allocItem..\n");
		memcpy((char*)ptt,buf,strlen(buf));
		raw_frame_que -> inQue((void *)ptt);
		printf("producer:inQue..\n");
		i ++;
	}
	free(buf);
	fclose(fp);
}
void * consumer(void *ptr)
{
	void *ptt;
	char ret[4];
	char *buf = (char*)malloc(SIZE);
	int i = 0;
	while(i < 300)
	{
		ptt = (raw_frame_que -> outQue());
		printf("consumer:outQue..\n");
		memcpy(buf,ptt,SIZE);
		printf("ret len is %d\n",strlen(buf));
		alloc_buf_que->freeItem((void*)ptt);
		printf("consumer:freeItem..\n");
		i ++;
	}
}

int main()
{
	pthread_t producer_t;
	pthread_t consumer_t;

	create_que();

	pthread_create(&producer_t,NULL,producer,NULL);
	pthread_create(&consumer_t,NULL,consumer,NULL);
	
	pthread_join(producer_t,NULL);
	pthread_join(consumer_t,NULL);
	return 0;
}
