#ifndef _fixed_allocator_h_
#define _fixed_allocator_h_


#include <pthread.h>
#include <list>

//#include "mt_queue.h"

using namespace std;

class FixedAllocator {
private:
    pthread_mutex_t  alloc_mutex;
    pthread_cond_t   alloc_cond;

    void*            arena;
    int              capacity;
    int              item_size;

    typedef  void* INT_ITTEM_P;
	
    list<INT_ITTEM_P>      free_list;
	
public:
    FixedAllocator(int _item_num, int _item_size);
    ~FixedAllocator();

    void* allocItem();
    void  freeItem(void* item);

};

// global singlton for Fixed Size Memory Allocator
extern FixedAllocator * alloc_buf_que;


#endif

